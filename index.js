'use strict';

const CHANNEL_NAME = "channel";


var redis = require('redis');
var client = redis.createClient();


function handleMessage(timeout) {

	var counter = 0;
	var messagesList = [];
	var currentTimeout;

	function _printMessage() {
		var message = messagesList.pop();
		var isQuit = message.toLowerCase() === "quit";

		console.log("Message " + ++counter + ": " + message);

		currentTimeout = null;
		if (isQuit) {
			client.unsubscribe(CHANNEL_NAME);
			process.exit(0);
		} else if (messagesList.length) {
			printMessages();
		} else {
			console.log("No more messages. Waiting...");
		}
	}

	function printMessages() {
		if (currentTimeout) return;
		currentTimeout = setTimeout(_printMessage, timeout || 2000);
	}

	return function (message) {
		// Push the message to internal buffer and start the actual handler
		messagesList.unshift(message);
		printMessages();
	};
}


var messageHandler = handleMessage(5000);


client.on('error', function (err) {
	console.log('Error: ' + err);
});


client.on('message', function (channel, messageBuffer) {
	messageHandler(messageBuffer.toString());
});


client.subscribe(CHANNEL_NAME);
